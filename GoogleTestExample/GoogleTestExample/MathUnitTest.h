#pragma once
#include "pch.h"
#include "Math.h"
#include <string>
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class MathTest : public ::testing::Test {
protected:
	Math* math_;

	virtual void SetUp();     // gtest construction
	virtual void TearDown();  // gtest destruction

	// Addtwo numbers
	int addTwoNum(int number1, int number2);
	// Sub two numbers
	int subTwoNum(int number1, int number2);
	// Div two numbers
	int divTwoNum(int number1, int number2);
	// Multi two numbers
	int mulTwoNum(int number1, int number2);
	// Factorial
	int factNum(int number1);
};